
# rabbit mq 설정
rabbit mq 연동을 위해서는 프로퍼티 파일을 변경하고 자바 주석을 해제한다. 자바파일은 목적에 맞게 변경하거나 추가하면 된다.
1. rabbit exchage, queue 설계
2. springboot 파일 설정 및 주석 해제
    - appliation.yml
    - RabbitMQConfiguration.java
    - BroadcastMessageProducer.java
    - BroadcastMessageConsumers.java

---
## 1. rabbit exchage, queue 설계
 rabbit mq를 사용하기 위해서 맨 먼저 exchage와 queue 를 설계하고 바인딩(route key)을 해 주어야 한다.   
 exchange 는 producer가 메시지를 생성하여 저장하는 곳이고 queue는 consumer가 메시지를 가져가기 위한 용도이다. route key는 특정 조건을 부여하여 exchange와 queue를 연결해 주는데 사용한다. exchage 타입에 따라 처리 방식이 달라진다. 

```bash
// 예제 샘플용 생성 명칭
exchagne : direct.exchange 로 생성
queue : charge.hello.queue 로 생성 
route : charge.hello.route 로 생성 
```

## 2. springboot 파일 설정 및 주석 해제
### appliation.yml
mq server 접속 정보와 amqp server 정보를 변경하다.
```properties
  rabbitmq:
    host: 169.56.80.70
    port: 30179
    username: user
    password: qRYmCjajJ9
```

rabbitmq 관리자 사이트에서 생성한 exchange, querue, route 정보를 변경한다.
```properties
prop: 
  rabbit:
    direct: 
      exchange: "direct.exchange"
    queue:
      order: "charge.hello.queue"
    route:
      order: "charge.hello.route"

```

### RabbitMQConfiguration.java
전체 주석을 해제하고 아래 디폴트 객체를 변경한다.
```java
idClassMapping.put("com.springboot.microservices.sample.rabbitmq", RabbitMessage.class);  // 기본 전문 클래스
```

### BroadcastMessageProducer.java
메시지를 생성하는 프로듀서 파일이다. exchange와 route key가 사용되고 전달할 메시지 객체를 input으로 한다. 초기 설정시 자동으로 객체가 json 타입으로 변경되어 rabbitmq에 전달된다. 비즈니스 로직에 따라 메소드를 추가하여 사용한다.  
예제에서는 아래 주석을 해제 하면 된다.
```java
// rabbitTemplate.convertAndSend(EXCHANGE, ROUTING_KEY, user);
```

### BroadcastMessageConsumers.java
메시지를 이용하는 컨슈머 파일이다. queue 정보가 사용되고 전달된 메시지 객체를 input의 객체 타입과 동일하게 한다. 전달받은 객체를 가지고 비즈니스 로직을 구현하여 사용하면 된다.   
예제는 아래 주석을 해제하여 사용하면 된다.
```java
@RabbitListener(queues = "${prop.rabbit.queue.hello}" )
public void receiveMessageFromDirectExchangeWithOrderQueue(SampleUser message) {
    log.debug("CHARGE_ORDER_QUEUE Receive : "+message.toString());
    //todo data를 수신해서 로직 처리 
};
```