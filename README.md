# mvp-springboot-sample
샘플 프로젝트는 2개로 구성된다. 
```
sample-service : msa 프로젝트 샘플용
sample-service6 : 처음 스프링부트를 사용하는 개발자가 용
```
   

### 프로그램 설치
---
#### java install
Java 1.8 이상 버전을 설치한다.   
Java 런타임 : [JRE](https://java.com/ko/download/ )  
Java SE : [Java SE](https://www.oracle.com/technetwork/java/javase/downloads/index.html)  
=> 자바 런타임으로 프로젝트를 실행 할 수 없는 경우 설치 

#### Spring Tool Suite install
스프링 부트 전용 IDE 설치한다.  
다운로드 주소 : [Spring.io](https://spring.io/tools)  

#### git install
git 프로그램을 로컬 컴퓨터에 설치한다.   
다운로드 주소 : [git.com](https://git-scm.com/)

#### lobok 설치
Lombok은 자바 클래스에서 사용되는 getter 또는 setter와 같은 boilerplate 사용을 줄이기 위해 사용하는 라이브러리로 이클립스에 의존성을 추가해야 한다.  
다운로드 주소 : [lombok.jar](https://projectlombok.org/download )

#### db client tool 설치
mysql 접속 전용 클라이언트 DB tool 을 설치한다.   
다운로드 주소 : [HeidiSQL](https://www.heidisql.com/)   


### git 저장소 로컬에 복사
---
#### sample 프로젝트 복사 
gitlab에서 스프링 부트 sample 프로젝트를 로컬 저장소로 복사한다.   
```bash
git clone https://gitlab.com/happycloudpak/mvp-springboot-sample.git
```
#### git 명령어 
로컬의 파일을 수정하고 커밋하기 위해 자주 사용되는 git 명령어이다.   
수정된 파일을 저장하기 위해 변경한 파일을 스테이징 상태로 변경하고 로컬 저장소에 커밋을 한다.  
로컬 저정소에 커밋된 파일은 일괄적으로 원격 저장소에 push한다.
```bash
git add file_name  # fine_name 을 스테이징 상태로 변경한다.  스테이징 된 상태만  commit 할 수 있다.
git commit -m '수정된 내용을 기록해 주세요'  # 로컬저장소에 변경된 소스를 commit 한다.
git push origin master  # local의 저장소를 원격 저장소로 push 한다.
```
### 기타
---
#### DB / 로그 설정정보 변경
application.yml 파일 정보의 설정 정보를 변경한다. database의 접속 정보 또는 로그 출력 정보를 수정한다.
```yaml
  datasource:
    driverClassName: com.mysql.jdbc.Driver
    url: jdbc:mysql://173.193.112.244:3306/msadb?useUnicode=true&characterEncoding=utf-8  # database 접속 정보 수정 
    username: msa # db접속 계정 정보변경 
    password: "passw0rd"
```


#### 디렉토리 구조
msa 프로젝트는 다음과 같은 디렉토리 구조를 갖는다.
```code
src
    main
        java
            msa~project~root/config // 환경 설정 정보 위치 
            msa~project~root/rest   // rest api controller 위치
            msa~project~root/service    // 서비스 파일 위치
            msa~project~root/dao        // dao 파일 위치
            msa~project~root/model      // vo 객체 위치 
        resources // 어플리케이션 yaml 파일 위치 
            mybatis
                mapper // database xml 위치 
```